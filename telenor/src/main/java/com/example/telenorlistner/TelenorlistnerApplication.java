package com.example.telenorlistner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TelenorlistnerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TelenorlistnerApplication.class, args);
    }

}
